let http = require('http');

let port = 4000;

const server = http.createServer(function(req,res){

    let url = req.url;
    let method = req.method;
    let msgResponse = '';

    if(url == '/' && method == 'GET'){
        msgResponse = 'Welcome to Booking System';
    }else if(url == '/profile' && method == 'GET'){
        msgResponse = 'Welcome to your profile';
    }else if(url == '/courses' && method == 'GET'){
        msgResponse = 'Here\'s our available courses';
    }else if(url == '/addcourse' && method == 'POST'){
        msgResponse = 'Add course to our resources';

		let request_body = '';
        req.on('data', function(data){
			request_body += data
		})

        req.on('end', function() {

            request_body = JSON.parse(request_body)

			let course = {
				"coursename": request_body.coursename,
				"courseId": request_body.courseId
			}
			
			res.writeHead(200, {'Content-Type': 'application/json'})
			res.write(JSON.stringify(course))
			res.end()

		})

    }else if(url == '/updatecourse' && method == 'PUT'){
        msgResponse = 'Update a course to our resources';
    }else if(url == '/archivecourse' && method == 'DELETE'){
        msgResponse = 'Archive courses to our resources';
    }

    if(url != '/addcourse'){
        res.writeHead(200,{'Content-Type':'text/plain'})
        res.end(msgResponse);
    }

    


})


server.listen(port);

console.log(`Server is running at localhost: ${port}`);